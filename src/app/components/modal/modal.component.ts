import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent {
  @Input() title: string = '';
  @Input() message: string = '';
  @Input() showModal: boolean = false;
  @Input() color: string = '#007bff';
  @Output() close = new EventEmitter<void>();

  onClose(): void {
    this.close.emit();
  }
}
