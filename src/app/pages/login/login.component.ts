import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm!: FormGroup;
  modalTitle: string = '';
  modalMessage: string = '';
  modalColor: string = '#007bff';
  showModal: boolean = false;

  constructor(private fb: FormBuilder, private router: Router, private authService: AuthService) { }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  onSubmit(): void {
    if (this.loginForm.valid) {
      const { username, password } = this.loginForm.value;
      if (this.authService.login(username, password)) {
        this.router.navigate(['/employee-list'], { state: { passDataIsLoggin: true } });
      } else {
        this.modalNotification()
      }
    } else {
      this.loginForm.markAllAsTouched();
    }
  }

  modalNotification(): void {
    this.modalTitle = "Alert"
    this.modalMessage = "Invalid Credentials"
    this.modalColor = "red"
    this.showModal = true;
  }

  closeModal(): void {
    this.showModal = false;
  }
}