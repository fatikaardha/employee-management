import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { EmployeeService } from '../../services/employee/employee.service';
import { Employee } from '../../models/employee.model';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {
  addEmployeeForm: FormGroup;
  today: string = new Date().toISOString().split('T')[0];
  groupSearchTerm: string = '';
  groups: string[] = [
    'Group 1', 'Group 2', 'Group 3', 'Group 4', 'Group 5',
    'Group 6', 'Group 7', 'Group 8', 'Group 9', 'Group 10'
  ];
  filteredGroups: string[] = this.groups;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private employeeService: EmployeeService,
    private authService: AuthService,
  ) {
    this.addEmployeeForm = this.fb.group({
      username: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      birthDate: ['', Validators.required],
      basicSalary: ['', [Validators.required, Validators.pattern(/^\d+$/)]],
      status: ['', Validators.required],
      group: ['', Validators.required],
      description: ['', Validators.required]
    });
  }

  ngOnInit() {
    if (!this.authService.isLoggedIn()) {
      this.router.navigate(['/login']);
      return;
    }
  }

  filterGroups(event: any): void {
    const searchTerm = event.target.value.toLowerCase();
    this.filteredGroups = this.groups.filter(group =>
      group.toLowerCase().includes(searchTerm)
    );
  }

  onSubmit(): void {
    if (this.addEmployeeForm.valid) {
      const newEmployee: Employee = this.addEmployeeForm.value;
      this.employeeService.addEmployee(newEmployee);
      this.router.navigate(['/employee-list'], { state: { passDataIsAdded: true } });
    } else {
      this.addEmployeeForm.markAllAsTouched();
    }
  }

  onCancel(): void {
    this.router.navigate(['/employee-list']);
  }
}
