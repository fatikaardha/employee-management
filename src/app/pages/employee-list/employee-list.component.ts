import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EmployeeService } from '../../services/employee/employee.service';
import { Employee } from '../../models/employee.model';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})

export class EmployeeListComponent implements OnInit {
  employees: Employee[] = [];
  filteredEmployees: Employee[] = [];
  searchUsernameOrEmail: string = '';
  searchGroup: string = '';
  currentPage: number = 1;
  itemsPerPage: number = 5;
  itemsPerPageOptions: number[] = [5, 10, 20];
  showModal: boolean = false;
  modalTitle: string = '';
  modalMessage: string = '';
  modalColor: string = '#007bff';
  passDataIsLoggin: boolean = false;
  passDataIsAdded: boolean = false;

  constructor(private employeeService: EmployeeService, private router: Router, private route: ActivatedRoute, private authService: AuthService,) {
    const navigation = this.router.getCurrentNavigation();
    console.log(navigation, 'navigation')
    this.passDataIsLoggin = navigation?.extras?.state?.['passDataIsLoggin'];
    this.passDataIsAdded = navigation?.extras?.state?.['passDataIsAdded'];
    this.searchGroup = navigation?.extras?.state?.['passDataSearch']?.searchGroup;
    this.searchUsernameOrEmail = navigation?.extras?.state?.['passDataSearch']?.searchUsernameOrEmail;
  }

  ngOnInit(): void {
    if (!this.authService.isLoggedIn()) {
      this.router.navigate(['/login']);
      return;
    }

    this.employees = this.employeeService.getEmployees();
    this.filteredEmployees = this.employees

    this.isInputSearchExist();
    this.handleLoginNotification();
    this.handleAddEmployeeNotification();
  }

  sort(field: string): void {
    this.filteredEmployees.sort((a: any, b: any) => {
      if (a[field] < b[field]) return -1;
      if (a[field] > b[field]) return 1;
      return 0;
    });
  }

  search(): void {
    this.filteredEmployees = this.employees.filter(employee =>
      employee.username.includes(this.searchUsernameOrEmail) &&
      employee.group.includes(this.searchGroup)
    );
  }

  notify(action: string, employee: Employee): void {
    this.modalTitle = action === 'edit' ? 'Success Edited' : 'Success Deleted';
    this.modalMessage = action === 'edit' ? `${employee.username} has been edited successfully!` : `${employee.username} has been deleted successfully!`;
    this.modalColor = action === 'edit' ? '#eab308' : '#dc2626';
    this.showModal = true;
  }

  navigateToAddEmployee(): void {
    this.router.navigate(['/add-employee']);
  }

  prevPage(): void {
    if (this.currentPage > 1) {
      this.currentPage--;
    }
  }

  nextPage(): void {
    if ((this.currentPage * this.itemsPerPage) < this.filteredEmployees.length) {
      this.currentPage++;
    }
  }

  viewDetail(employee: Employee): void {
    this.router.navigate([`/employee-detail/${employee.id}`], { state: { searchUsernameOrEmail: this.searchUsernameOrEmail, searchGroup: this.searchGroup } });
  }

  closeModal(): void {
    this.showModal = false;
  }

  logout(): void {
    this.authService.logout()
  }

  deleteEmployee(employee: Employee): void {
    this.filteredEmployees = this.employeeService.deleteEmployee(employee.id, this.filteredEmployees);
    this.notify('delete', employee);
  }

  handleLoginNotification(): void {
    if (this.passDataIsLoggin && !localStorage.getItem('hasLoggedIn')) {
      this.modalTitle = "Successfully Login";
      this.showModal = true;
      localStorage.setItem('hasLoggedIn', 'true');
    }
  }

  handleAddEmployeeNotification(): void {
    if (this.passDataIsAdded && !localStorage.getItem('dataIsAdded')) {
      this.modalTitle = "Success";
      this.modalMessage = "Successfully added data."
      this.showModal = true;
      localStorage.setItem('dataIsAdded', 'true');
    }
  }

  isInputSearchExist(): void {
    if (this.searchUsernameOrEmail.length > 0 || this.searchGroup.length > 0) {
      this.filteredEmployees = this.employees.filter(employee =>
        employee.username.includes(this.searchUsernameOrEmail) &&
        employee.group.includes(this.searchGroup)
      );
    }
  }
}
