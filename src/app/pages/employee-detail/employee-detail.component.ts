import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeService } from '../../services/employee/employee.service';
import { Employee } from '../../models/employee.model';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.css']
})
export class EmployeeDetailComponent implements OnInit {
  employee: Employee | undefined;
  passDataSearch: any = {}

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private employeeService: EmployeeService,
    private authService: AuthService,
  ) {
    const navigation = this.router.getCurrentNavigation();
    this.passDataSearch.searchGroup = navigation?.extras?.state?.['searchGroup']
    this.passDataSearch.searchUsernameOrEmail = navigation?.extras?.state?.['searchUsernameOrEmail']
    console.log(this.passDataSearch)
  }

  ngOnInit() {
    if (!this.authService.isLoggedIn()) {
      this.router.navigate(['/login']);
      return;
    }

    const id = this.route.snapshot.paramMap.get('id');
    if (id !== null) {
      this.employee = this.employeeService.getEmployeeById(+id);
    }
  }

  goBack(): void {
    this.router.navigate(['/employee-list'], { state: { passDataSearch: this.passDataSearch } });
  }

  formatCurrency(amount: number): string {
    return `Rp. ${amount.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}`;
  }
}
