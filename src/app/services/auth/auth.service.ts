import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private router: Router) { }

  login(username: string, password: string): boolean {
    if (username === 'admin' && password === 'admin') {
      localStorage.setItem('dummyToken', 'your_dummy_token');
      return true;
    }
    return false;
  }

  logout(): void {
    localStorage.removeItem('dummyToken');
    localStorage.removeItem('hasLoggedIn');
    localStorage.removeItem('dataIsAdded');
    this.router.navigate(['/login']);
  }

  isLoggedIn(): boolean {
    return localStorage.getItem('dummyToken') !== null;
  }
}
