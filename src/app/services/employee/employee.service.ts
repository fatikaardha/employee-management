import { Injectable } from '@angular/core';
import { Employee } from '../../models/employee.model';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  private employees: Employee[] = [];

  constructor() {
    this.generateDummyData();
  }

  generateDummyData(): void {
    for (let i = 1; i <= 100; i++) {
      this.employees.push({
        id: i,
        username: `user${i}`,
        firstName: `First${i}`,
        lastName: `Last${i}`,
        email: `user${i}@example.com`,
        birthDate: new Date(1990, 0, i),
        basicSalary: 50000 + i * 1000,
        status: 'Active',
        group: `Group${i % 10}`,
        description: `Description for user${i}`
      });
    }
  }

  getEmployees(): Employee[] {
    return this.employees;
  }

  getEmployeeById(id: number): Employee | undefined {
    return this.employees.find(employee => employee.id === id);
  }

  addEmployee(employee: Employee): void {
    this.employees.unshift(employee);
  }

  deleteEmployee(id: number, filteredEmployees: Employee[]) {
    return filteredEmployees.filter((item: Employee) => item.id !== id);
  }
}
